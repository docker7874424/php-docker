FROM php:8.1-fpm-alpine3.16

COPY --from=composer:2.5 /usr/bin/composer /usr/local/bin/composer

RUN apk add --no-cache autoconf libzip libzip-dev unzip icu-dev build-base
RUN docker-php-ext-install \
    intl \
    pdo_mysql \
    zip \
    && pecl install apcu-5.1.21 redis \
    && docker-php-ext-enable apcu redis \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip

ENV APP_ENV=
ENV APP_SECRET=
ENV DATABASE_DRIVER=
ENV DATABASE_DBNAME=
ENV DATABASE_HOST=
ENV DATABASE_PORT=
ENV DATABASE_USER=
ENV DATABASE_PASSWORD=
ENV DATABASE_SERVER_VERSION=

WORKDIR /var/www/service
COPY --chown=www-data:www-data . .
USER www-data
RUN composer service:build
